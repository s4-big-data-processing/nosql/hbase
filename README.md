# Practical Task: HBase

Author: [Ziyodulla Baykhanov](https://www.linkedin.com/in/ziyodulla-baykhanov/)

Date: 5th of May, 2024

Email: [ziyodulla_baykhanov@student.itpu.uz](mailto:ziyodulla_baykhanov@student.itpu.uz)

## Installation and setting up the environment

### Creating a virtual Docker network

Run command in terminal to create a virtual Docker network:

```bash
docker network create bigdata_network
```

![image](screenshots/Screenshot01.png)

Inspect the network to find the gateway's IP address and mark the IP address, which in my case is `172.18.0.1`:

```bash
docker network inspect bigdata_network
```

![image](screenshots/Screenshot02.png)

### Creating the HBase Docker container

To install the Docker container for the HBase practice, run the following command:

```bash
docker create --name hbase --network bigdata_network -e ADVERTISED_HOST=172.18.0.1 -it ofrir119/hbase:1.0.0
```

![image](screenshots/Screenshot03.png)

### Starting the HBase Docker container

Start the Docker container using the following command:

```bash
docker start hbase
```

Verify the "HBase" environment is up and running:

```bash
docker ps -a
```

![image](screenshots/Screenshot04.png)

Open a BASH session to the practice environment.

```bash
docker exec -it hbase /bin/bash
ls -l
```

![image](screenshots/Screenshot05.png)

## 1. General details

1. View the HBase related scripts in the HBase `bin` directory. Provide both the command and execution results.

    - Hint: The location of the base directory should be in `/opt`.
    - Hint: You should see ~25 scripts.

    ```bash
    # 1. Viewing HBase related scripts in the HBase `bin` directory
    cd /opt/hbase/bin
    ls -l
    ```

    ![image](screenshots/Screenshot06.png)

2. Enter HBase Shell. Provide both the command and execution results.

    ```bash
    # 2. Entering HBase Shell
    hbase shell
    ```

    ![image](screenshots/Screenshot07.png)

3. Get the HBase status in different levels (base status, simple status, and detailed status). Provide both the command and execution results.

    ```bash
    # Base status provides a summary of the cluster state.
    status

    # Simple status provides a concise overview of the cluster.
    status 'simple'

    # Detailed status provides an in-depth look at the cluster, including regions.
    status 'detailed'
    ```

    ![image](screenshots/Screenshot08.png)

4. List all filters. Provide both the command and execution results.

    ```bash
    # Listing all filters in hbase
    show_filters
    ```

    ![image](screenshots/Screenshot09.png)  

5. List all tables. Provide both the command and execution results.

    ```bash
    # Listing all tables in hbase
    list
    ```

    ![image](screenshots/Screenshot10.png)

## 2. Table and Data Creation

1. Create an `employees` table with the following column families. Provide both the command and execution results.

    - `personal_data`: stores 2 versions for this column
    - `professional_data`: stores 4 versions for this column

    ```bash
    create 'employees', {NAME => 'personal_data', VERSIONS => 2}, {NAME => 'professional_data', VERSIONS => 4}
    ```

    ![image](screenshots/Screenshot11.png)

2. List all tables. Provide both the command and execution results.

    ```bash
    list
    ```

    ![image](screenshots/Screenshot12.png)

3. Generate and insert data for ten employees and check that data is inserted. Provide both the command and execution results.

    - The `id` of each employee must be a unique value.
    - Insert employee's id to be 1-10, fill the following data:
        - personal_data: first_name, surname, age
        - professional_data: role, expertise

    ```bash
    # 3. Inserting data for ten employees
    put 'employees', 'emp01', 'personal_data:first_name', 'Alice'
    put 'employees', 'emp01', 'personal_data:surname', 'Smith'
    put 'employees', 'emp01', 'personal_data:age', 30
    put 'employees', 'emp01', 'professional_data:role', 'Developer'
    put 'employees', 'emp01', 'professional_data:expertise', 'Java'

    put 'employees', 'emp02', 'personal_data:first_name', 'Bob'
    put 'employees', 'emp02', 'personal_data:surname', 'Johnson'
    put 'employees', 'emp02', 'personal_data:age', 35
    put 'employees', 'emp02', 'professional_data:role', 'Manager'
    put 'employees', 'emp02', 'professional_data:expertise', 'Business Management'

    put 'employees', 'emp03', 'personal_data:first_name', 'Charlie'
    put 'employees', 'emp03', 'personal_data:surname', 'Williams'
    put 'employees', 'emp03', 'personal_data:age', 25
    put 'employees', 'emp03', 'professional_data:role', 'Designer'
    put 'employees', 'emp03', 'professional_data:expertise', '3D Design'

    put 'employees', 'emp04', 'personal_data:first_name', 'David'
    put 'employees', 'emp04', 'personal_data:surname', 'Brown'
    put 'employees', 'emp04', 'personal_data:age', 40
    put 'employees', 'emp04', 'professional_data:role', 'Engineer'
    put 'employees', 'emp04', 'professional_data:expertise', 'Mechanical Engineering'

    put 'employees', 'emp05', 'personal_data:first_name', 'Eva'
    put 'employees', 'emp05', 'personal_data:surname', 'Davis'
    put 'employees', 'emp05', 'personal_data:age', 20
    put 'employees', 'emp05', 'professional_data:role', 'Analyst'
    put 'employees', 'emp05', 'professional_data:expertise', 'Business Analysis'

    put 'employees', 'emp06', 'personal_data:first_name', 'Frank'
    put 'employees', 'emp06', 'personal_data:surname', 'Miller'
    put 'employees', 'emp06', 'personal_data:age', 33
    put 'employees', 'emp06', 'professional_data:role', 'Software Architect'
    put 'employees', 'emp06', 'professional_data:expertise', 'Software Architecture'

    put 'employees', 'emp07', 'personal_data:first_name', 'Grace'
    put 'employees', 'emp07', 'personal_data:surname', 'Wilson'
    put 'employees', 'emp07', 'personal_data:age', 38
    put 'employees', 'emp07', 'professional_data:role', 'Consultant'
    put 'employees', 'emp07', 'professional_data:expertise', 'Consulting'

    put 'employees', 'emp08', 'personal_data:first_name', 'Henry'
    put 'employees', 'emp08', 'personal_data:surname', 'Moore'
    put 'employees', 'emp08', 'personal_data:age', 27
    put 'employees', 'emp08', 'professional_data:role', 'Auditor'
    put 'employees', 'emp08', 'professional_data:expertise', 'Auditing'

    put 'employees', 'emp09', 'personal_data:first_name', 'Ivy'
    put 'employees', 'emp09', 'personal_data:surname', 'Taylor'
    put 'employees', 'emp09', 'personal_data:age', 42
    put 'employees', 'emp09', 'professional_data:role', 'Lawyer'
    put 'employees', 'emp09', 'professional_data:expertise', 'Law'

    put 'employees', 'emp10', 'personal_data:first_name', 'Jack'
    put 'employees', 'emp10', 'personal_data:surname', 'Anderson'
    put 'employees', 'emp10', 'personal_data:age', 22
    put 'employees', 'emp10', 'professional_data:role', 'Doctor'
    put 'employees', 'emp10', 'professional_data:expertise', 'Medicine'
    ```

    ![image](screenshots/Screenshot13.png)
    ![image](screenshots/Screenshot14.png)
    ![image](screenshots/Screenshot15.png)

4. Scan employee table to print all rows. Provide both the command and execution results.

    ```bash
    scan 'employees'
    ```

    ![image](screenshots/Screenshot16.png)
    ![image](screenshots/Screenshot17.png)

5. Get all data of employee with id 7. Provide both the command and execution results.

    ```bash
    get 'employees', 'emp07'
    ```

    ![image](screenshots/Screenshot18.png)

6. Update age and role of employee number 3 and make sure updates applied. Provide both the command and execution results.

    ```bash

    # View the data of employee with id 3 before updating
    get 'employees', 'emp03'

    # Updating age and role of employee number 3
    put 'employees', 'emp03', 'personal_data:age', 28
    put 'employees', 'emp03', 'professional_data:role', 'Artist'

    # View the data of employee with id 3 after updating
    get 'employees', 'emp03'
    ```

    ![image](screenshots/Screenshot19.png)

## 3. Query Data

1. Query all records in `employees` table. Provide both the command and execution results.

    ```bash
    scan 'employees'
    ```

    ![image](screenshots/Screenshot20.png)
    ![image](screenshots/Screenshot21.png)

2. Get all data of employee with id 3 and the 3 last versions of his column families: personal_data, professional_data. Provide both the command and execution results.

    ```bash
    get 'employees', 'emp3', {COLUMN => ['personal_data', 'professional_data'], VERSIONS => 3}
    ```

    ![image](screenshots/Screenshot22.png)

3. Get all data of employees with age bigger or equals to 40. Provide both the command and execution results.

    ```bash
    scan 'employees', {FILTER => "SingleColumnValueFilter('personal_data','age', >=, 'binary:40', true, true)"}
    ```

    ![image](screenshots/Screenshot23.png)

4. Get only role value of all employees with age bigger than 35. Provide both the command and execution results.

    ```bash
    # Get all data of employees with age bigger than 35
    scan 'employees', {FILTER => "SingleColumnValueFilter('personal_data','age', >, 'binary:35', true, true)"}

    # Get only role value of all employees with age bigger than 35
    scan 'employees', {FILTER => "(SingleColumnValueFilter('personal_data','age', >, 'binary:35', true, true) AND QualifierFilter(=, 'substring:role'))"}
    ```

    ![image](screenshots/Screenshot24.png)

5. Count the number of all employees. Provide both the command and execution results.

    ```bash
    count 'employees'
    ```

    ![image](screenshots/Screenshot25.png)

6. Count the number of employees with age less than 40. Provide both the command and execution results.

    ```bash
    # Count the number of employees with age less than 40
    count 'employees', {FILTER => "SingleColumnValueFilter('personal_data','age', <, 'binary:40', true, true)"}
    ```

    ![image](screenshots/Screenshot26.png)

    ```bash
    # Get only age value of all employees with age less than 40
    scan 'employees', {COLUMNS => ['personal_data:age'], FILTER => "SingleColumnValueFilter('personal_data','age', <, 'binary:40', true, true)"}
    ```

    ![image](screenshots/Screenshot27.png)

7. Delete the newer age (that updated in topic 4) for employee with id 3. Provide both the command and execution results.

    ```bash
    # First, get the data of employee with id 3
    get 'employees', 'emp03'

    # Second, find the timestamp of the newer age value
    get 'employees', 'emp03', {COLUMN => 'personal_data:age', VERSIONS => 2}

    # Delete the newer age value
    delete 'employees', 'emp03', 'personal_data:age', 1714949191045
    ```

    ![image](screenshots/Screenshot28.png)

8. Get the data of employee with id 3 and validate his age reverted to first value. Provide both the command and execution results.

    ```bash
    # Get the data of employee with id 3 and validate his age reverted to first value
    get 'employees', 'emp03'
    get 'employees', 'emp03', {COLUMN => 'personal_data:age'}
    ```

    ![image](screenshots/Screenshot29.png)

## 4. Delete Table

1. Export the "employees" table to a file.

    ```bash
    # Export the "employees" table to a file
    hbase org.apache.hadoop.hbase.mapreduce.Export 'employees' '/home/users'

    # We can import the data later (if needed) using the following command:
    hbase org.apache.hadoop.hbase.mapreduce.Import 'employees' '/home/users'
    ```

    ![image](screenshots/Screenshot30.png)
    ![image](screenshots/Screenshot31.png)

2. Delete "employees" table. Provide both the command and execution results.

```bash
# 2. Deleting "employees" table
disable 'employees'
drop 'employees'
```

![image](screenshots/Screenshot32.png)
